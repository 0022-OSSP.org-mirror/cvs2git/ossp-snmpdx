##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  sysProc.pm: probe for System Process Information
##

package My::Probe::sysProc;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.system.sysProc.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;
    
    if ($obj->{-name} =~ m|\.sysProcMax$|) {
        if ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i) {
            #   query system via sysctl(8)
            my $out = $self->{-ctx}->{-sys}->run("/sbin/sysctl -n kern.maxproc", "1d");
            $obj->{-value} = $out->{-stdout};
            $obj->{-value} =~ s/^\s*(.+?)\s*$/$1/s;
        }
        elsif ($self->{-ctx}->{-platform}->id() =~ m/Linux/i) {
            #   query system via /proc or sysctl(8)
            my $out = $self->{-ctx}->{-sys}->run(
                "cat /proc/sys/kernel/threads-max 2>&1 || " .
                "/sbin/sysctl kernel/threads-max", "1d");
            $obj->{-value} = $out->{-stdout};
            $obj->{-value} =~ s/^[^\d]*(\d+).*$/$1/s;
        }
        elsif ($self->{-ctx}->{-platform}->id() =~ m/SunOS/i) {
            #   query system via sysdef(8)
            my $out = $self->{-ctx}->{-sys}->run("/usr/sbin/sysdef -i", "1d");
            if ($out->{-stdout} =~ m/\s*(\d+)\s+maximum number of processes/s) {
                $obj->{-value} = $1;
            }
        }
    }
    elsif ($obj->{-name} =~ m|\.sysProcActive$|) {
        #   query system via ps(1)
        my $ps = "ps";
        if    ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i) { $ps = "/bin/ps -ax"; }
        elsif ($self->{-ctx}->{-platform}->id() =~ m/Linux/i)   { $ps = "/bin/ps ax";  }
        elsif ($self->{-ctx}->{-platform}->id() =~ m/SunOS/i)   { $ps = "/bin/ps -e";  }
        my $out = $self->{-ctx}->{-sys}->run($ps, "1m");
        my @lines = split(/\n/, $out->{-stdout});
        $obj->{-value} = ($#lines+1);
    }
    return;
}

1;

