##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  sysLoad.pm: probe for System Load Average
##

package My::Probe::sysLoad;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.system.sysLoad.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;

    #   query system via uptime(1)
    my $uptime = "uptime";
    if ($self->{-ctx}->{-platform}->id() =~ m/(FreeBSD|Linux|SunOS)/i) {
        $uptime = "/usr/bin/uptime"
    }
    my $out = $self->{-ctx}->{-sys}->run($uptime, "1m");

    #   parse out details (example outputs follow)
    #   FreeBSD: "5:36PM  up 6 days, 54 mins, 9 users, load averages: 0.00, 0.02, 0.00"
    #   Linux:   "17:36:50 up 49 days,  8:16,  2 users,  load average: 0.00, 0.00, 0.00"
    #   SunOS:   "5:36pm  up 71 day(s),  3:29,  2 users,  load average: 0.16, 0.11, 0.13"
    my ($load5, $load10, $load15) = (0, 0, 0);
    if ($out->{-stdout} =~ m|load\s+averages?:\s+(\S+),\s+(\S+),\s+(\S+)|si) {
        ($load5, $load10, $load15) = ($1, $2, $3);
    }

    #   provide result
    if    ($obj->{-name} =~ m|\.sysLoad5$|)  { $obj->{-value} = sprintf("%d", $load5  * 100); }
    elsif ($obj->{-name} =~ m|\.sysLoad10$|) { $obj->{-value} = sprintf("%d", $load10 * 100); }
    elsif ($obj->{-name} =~ m|\.sysLoad15$|) { $obj->{-value} = sprintf("%d", $load15 * 100); }
    return;
}

1;

