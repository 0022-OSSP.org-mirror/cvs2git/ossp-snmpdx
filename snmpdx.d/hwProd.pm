##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  hwProd.pm: probe for Hardware, Product
##

package My::Probe::hwProd;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.hardware.hwProd.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;

    #   query hardware architecture via uname(1)
    my $uname = "uname";
    if    ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i)       { $uname = "/usr/bin/uname"; }
    elsif ($self->{-ctx}->{-platform}->id() =~ m/(Linux|SunOS)/i) { $uname = "/bin/uname";     }
    my $out = $self->{-ctx}->{-sys}->run("$uname -m 2>/dev/null || $uname -p 2>/dev/null", "1d");
    my $arch = $out->{-stdout};
    $arch =~ s|^\s*(.+?)\s*$|$1|s;

    #   provide results
    if ($obj->{-name} =~ m|\.hwProdArch$|) {
        $obj->{-value} = $arch;
    }
    elsif ($obj->{-name} =~ m|\.hwProdName$|) {
        my $name = "NoName";
        if ($self->{-ctx}->{-platform}->id() =~ m/SunOS/i) {
            my $out = $self->{-ctx}->{-sys}->run("/usr/platform/`uname -i`/sbin/prtdiag", "1d");
            if ($out->{-stdout} =~ m|System\s+Configuration:\s+(.*?)\n|si) {
                #  System Configuration:  Sun Microsystems  sun4u Sun (TM) Enterprise 250 (2 X UltraSPARC-II 400MHz)
                #  System Configuration:  Sun Microsystems  sun4u Netra t1 (UltraSPARC-IIi 440MHz)
                #  System Configuration:  Sun Microsystems  sun4u Sun Ultra 450 (2 X UltraSPARC-II 248MHz)
                #  System Configuration:  Sun Microsystems  sun4u Sun Enterprise 420R (2 X UltraSPARC-II 450MHz)
                $name = $1;
                $name =~ s|\s+$arch||s;
                $name =~ s|(Sun Microsystems)\s+Sun|$1|si;
                $name =~ s|\s+\(TM\)||si;
            }
        }
        elsif ($arch =~ m/^(i?[3-6x]86|.*(pentium|athlon).*)$/i) {
            $name .= "-PC";
            if ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i) {
                my $out = $self->{-ctx}->{-sys}->run("/sbin/sysctl -n hw.model", "1d");
                my $model = $out->{-stdout};
                $model =~ s/^\s*(.+?)\s*$/$1/s;
                $name .= " ($model)";
            }
        }
        $obj->{-value} = $name;
    }
    return;
}

1;

