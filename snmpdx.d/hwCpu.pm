##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  hwCpu.pm: probe for Hardware, CPU
##

package My::Probe::hwCpu;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.hardware.hwCpu.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;

    if ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i) {
        my $hwCpuNum;
        my @hwCpuENTRY; #N/A

        #   local workspace
        my $out; my $raw;

        #   query sysctl(8)
        $out = $self->{-ctx}->{-sys}->run("/sbin/sysctl -n hw.ncpu", "forever");
        $raw = $out->{-stdout};
        if ($raw =~ m/^\s*(.+?)\s*$/s) {
            $hwCpuNum = $1;
        }

        $obj->{-value} = $hwCpuNum if ($obj->{-name} =~ m/\.hwCpuNum$/);
    }
    elsif ($self->{-ctx}->{-platform}->id() =~ m/Linux/i) {
        my $hwCpuNum;
        my @hwCpuENTRY; #N/A

        #   local workspace
        my $out; my $raw;

        #   query dmesg(8)
        #   FIXME: this works only within a reasonable time after booting
        #          but I don't know any alternative method for querying the speed!
        $out = $self->{-ctx}->{-sys}->run("/bin/dmesg", "forever");
        $raw = $out->{-stdout};
        if ($raw =~ m/\nProcessors: (\d+)\n/si) {
            $hwCpuNum = $1;
        }

        $obj->{-value} = $hwCpuNum if ($obj->{-name} =~ m/\.hwCpuNum$/);
    }
    elsif($self->{-ctx}->{-platform}->id() =~ m/SunOS/i) {
        my $hwCpuNum;
        my @hwCpuENTRY;

        #   local workspace
        my $out; my $raw; my $arch;

        #   initialize arch
        $arch = $self->{-ctx}->{-platform}->arch();

        #   hwCpuSpeed
        $out = $self->{-ctx}->{-sys}->run("/usr/platform/$arch/sbin/prtdiag -v", "1m");
        $raw = $out->{-stdout};
        if ($raw =~ m/\n=+ CPUs =+\n.*?\n---[ -]+\n(.*?)\n=+/s) {
            $raw = $1;
            foreach $line (split "\n", $raw) {
                my ($brd,$cpu,$module,$mhz,$mb,$impl,$mask) = split(" ", $line);
                if ($cpu =~ m/^\d+$/) {
                    $hwCpuENTRY[$cpu] = {} if (not defined $hwCpuENTRY[$cpu]);
                    $hwCpuENTRY[$cpu]->{hwCpuIndex} = $cpu;
                    $hwCpuENTRY[$cpu]->{hwCpuId}    = "Cpu_$cpu";
                    $hwCpuENTRY[$cpu]->{hwCpuSpeed} = $mhz;
                }
            }
        }

        #   hwCpuTemp
        $out = $self->{-ctx}->{-sys}->run("/usr/platform/$arch/sbin/prtdiag -v", "1m");
        $raw = $out->{-stdout};
        if ($raw =~ m/\n=+ Environmental Status =+\n.*\nSystem Temperatures \(Celsius\):\n-+\n(.*?)\n=+/s) {
            $raw = $1;
            foreach $line (split "\n", $raw) {
                my ($cpu,$temp) = split(" ", $line);
                if ($cpu =~ m/CPU(\d+)/) {
                    $cpu = $1;
                    $new->{$cpu} = $cpu;
                    $hwCpuENTRY[$cpu] = {} if (not defined $hwCpuENTRY[$cpu]);
                    $hwCpuENTRY[$cpu]->{hwCpuIndex} = $cpu;
                    $hwCpuENTRY[$cpu]->{hwCpuId}    = "Cpu_$cpu";
                    $hwCpuENTRY[$cpu]->{hwCpuTemp}  = $temp;
                }
            }
        }

        #   hwCpuNum
        $hwCpuNum = $#{@hwCpuENTRY} + 1;
        
        $obj->{-value} = $hwCpuNum   if ($obj->{-name} =~ m/\.hwCpuNum$/);
        if ($obj->{-name} =~ m/\.hwCpuENTRY\.([^.]+)\.(\d+)$/) {
            my $col = $1;
            my $row = ($2 > 0 ? $2 - 1 : 0);
            $self->{-ctx}->{-log}->printf(4, "col=%s row=%s", $col, $row);
            $obj->{-value} = $hwCpuENTRY[$row]->{$col} if (defined $hwCpuENTRY[$row] and defined $hwCpuENTRY[$row]->{$col});
        }
    }
    return;
}

1;

