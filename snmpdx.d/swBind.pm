##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  swBind.pm: probe for Software, Bind (aka named)
##

package My::Probe::swBind;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.software.swBind.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;
    my $openpkg = $self->{-ctx}->{-opt}->{openpkg};

    #   local workspace
    my $out; my $raw;
    my $rndc = "$openpkg/sbin/rndc";
    my $file = "$openpkg/var/bind/named.stats"; # "statistics-file" setting in "option" section in named.conf

    #   query status via rndc(8)
    $out = $self->{-ctx}->{-sys}->run("$rndc status 2>/dev/null", "1m");
    $raw = $out->{-stdout};
    my $swBindStatusNumberOfZones    = $1 if ($raw =~ m/number of zones: (\d+)/s         );
    my $swBindStatusDebugLevel       = $1 if ($raw =~ m/debug level: (\d+)/s             );
    my $swBindStatusXfersRunning     = $1 if ($raw =~ m/xfers running: (\d+)/s           );
    my $swBindStatusXfersDeferred    = $1 if ($raw =~ m/xfers deferred: (\d+)/s          );
    my $swBindStatusSoaQueries       = $1 if ($raw =~ m/soa queries in progress: (\d+)/s );
    my $swBindStatusQueryLogging     = $1 if ($raw =~ m/query logging is (^\n)+/s        );
    my $swBindStatusRecClientsActive = $1 if ($raw =~ m/recursive clients: (\d+)\/\d+/s  );
    my $swBindStatusRecClientsMax    = $1 if ($raw =~ m/recursive clients: \d+\/(\d+)/s  );
    my $swBindStatusTcpClientsActive = $1 if ($raw =~ m/tcp clients: (\d+)\/\d+/s        );
    my $swBindStatusTcpClientsMax    = $1 if ($raw =~ m/tcp clients: \d+\/(\d+)/s        );

    ##   query stats via rndc(8)
    $self->{-ctx}->{-sys}->run("$rndc stats 2>/dev/null", "1m");
    $raw = "";
    open(STATS, "<$file");
    if (defined STATS) {
        while (<STATS>) {
            if (m/^\+\+\+/) {
                $raw = ""
            } else {
                $raw .= $_
            }
        }
        close(STATS);
    };
    my $swBindStatsSuccess   = $1 if ($raw =~ m/success (\d+)/s  );
    my $swBindStatsReferral  = $1 if ($raw =~ m/referral (\d+)/s );
    my $swBindStatsNxrrset   = $1 if ($raw =~ m/nxrrset (\d+)/s  );
    my $swBindStatsNxdomain  = $1 if ($raw =~ m/nxdomain (\d+)/s );
    my $swBindStatsRecursion = $1 if ($raw =~ m/recursion (\d+)/s);
    my $swBindStatsFailure   = $1 if ($raw =~ m/failure (\d+)/s  );

    #   provide results
    if    ($obj->{-name} =~ m|\.swBindStatusNumberOfZones$|   ) { $obj->{-value} = $swBindStatusNumberOfZones;    }
    elsif ($obj->{-name} =~ m|\.swBindStatusDebugLevel$|      ) { $obj->{-value} = $swBindStatusDebugLevel;       }
    elsif ($obj->{-name} =~ m|\.swBindStatusXfersRunning$|    ) { $obj->{-value} = $swBindStatusXfersRunning;     }
    elsif ($obj->{-name} =~ m|\.swBindStatusXfersDeferred$|   ) { $obj->{-value} = $swBindStatusXfersDeferred;    }
    elsif ($obj->{-name} =~ m|\.swBindStatusSoaQueries$|      ) { $obj->{-value} = $swBindStatusSoaQueries;       }
    elsif ($obj->{-name} =~ m|\.swBindStatusQueryLogging$|    ) { $obj->{-value} = $swBindStatusQueryLogging;     }
    elsif ($obj->{-name} =~ m|\.swBindStatusRecClientsActive$|) { $obj->{-value} = $swBindStatusRecClientsActive; }
    elsif ($obj->{-name} =~ m|\.swBindStatusRecClientsMax$|   ) { $obj->{-value} = $swBindStatusRecClientsMax;    }
    elsif ($obj->{-name} =~ m|\.swBindStatusTcpClientsActive$|) { $obj->{-value} = $swBindStatusTcpClientsActive; }
    elsif ($obj->{-name} =~ m|\.swBindStatusTcpClientsMax$|   ) { $obj->{-value} = $swBindStatusTcpClientsMax;    }
    elsif ($obj->{-name} =~ m|\.swBindStatsSuccess$|          ) { $obj->{-value} = $swBindStatsSuccess;           }
    elsif ($obj->{-name} =~ m|\.swBindStatsReferral$|         ) { $obj->{-value} = $swBindStatsReferral;          }
    elsif ($obj->{-name} =~ m|\.swBindStatsNxrrset$|          ) { $obj->{-value} = $swBindStatsNxrrset;           }
    elsif ($obj->{-name} =~ m|\.swBindStatsNxdomain$|         ) { $obj->{-value} = $swBindStatsNxdomain;          }
    elsif ($obj->{-name} =~ m|\.swBindStatsRecursion$|        ) { $obj->{-value} = $swBindStatsRecursion;         }
    elsif ($obj->{-name} =~ m|\.swBindStatsFailure$|          ) { $obj->{-value} = $swBindStatsFailure;           }
    return;
}

1;

