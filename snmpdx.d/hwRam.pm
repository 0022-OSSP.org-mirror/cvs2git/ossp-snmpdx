##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  hwRam.pm: probe for Hardware, RAM
##

package My::Probe::hwRam;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.hardware.hwRam.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;
    my $arch = $self->{-ctx}->{-platform}->arch();

    if ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i) {
        # hwRamMax    "/sbin/sysctl -n hw.physmem", convert bytes to MB
        # hwRamStatus N.A.
        #
        if ($obj->{-name} =~ m/\.hwRamMax$/) {
            my $hwRamMax;
            my $hwRamStatus;

            #   local workspace
            my $out; my $raw;

            #   hwRamMax
            $out = $self->{-ctx}->{-sys}->run("/sbin/sysctl -n hw.physmem", "forever");
            $raw = $out->{-stdout};
            if ($raw =~ m/^(\d+)$/) {
                $hwRamMax = int($1 / 1024 / 1024);
            }
            $obj->{-value} = $hwRamMax;
        }
    }
    elsif ($self->{-ctx}->{-platform}->id() =~ m/Linux/i) {
        # hwRamMax    "/bin/dmesg", Memory, convert KB to MB
        # hwRamStatus N.A.
        #
        if ($obj->{-name} =~ m/\.hwRamMax$/) {
            my $hwRamMax;
            my $hwRamStatus;

            #   local workspace
            my $out; my $raw;

            #   hwRamMax
            $out = $self->{-ctx}->{-sys}->run("/bin/dmesg", "forever");
            $raw = $out->{-stdout};
            if ($raw =~ m/\nMemory:\s+\d+k\/(\d+)k available/s) {
                $hwRamMax = int($1 / 1024);
            }
            $obj->{-value} = $hwRamMax;
        }
    }
    elsif ($self->{-ctx}->{-platform}->id() =~ m/SunOS/i) {
        # hwRamMax    "/usr/platform/$arch/sbin/prtdiag -v", Memory size
        # hwRamStatus N.A.
        #
        #FIXME set prtdiag cache expiry to "forever" when cache supports multiple caching times
        #
        if ($obj->{-name} =~ m/\.hwRam(Max|Status)$/) {
            my $hwRamMax;
            my $hwRamStatus;

            #   local workspace
            my $out; my $raw; my $n2i; my @dat; my $arch;

            #   initialize arch
            $arch = $self->{-ctx}->{-platform}->arch();

            #   hwRamMax
            $out = $self->{-ctx}->{-sys}->run("/usr/platform/$arch/sbin/prtdiag -v", "1m");
            $raw = $out->{-stdout};
            if ($raw =~ m/\nMemory size: (\d+) Megabytes/s) {
                $hwRamMax = $1;
            }

            #   hwRamStatus
            $out = $self->{-ctx}->{-sys}->run("/usr/platform/$arch/sbin/prtdiag -v", "1m");
            $raw = $out->{-stdout};
            if ($raw =~ m/\n=+ Memory =+\n(.*?)\n=+/s) {
                $raw = $1;
                $hwRamStatus = 1;
                foreach $line (split "\n", $raw) {
                    my ($bank, $interleavegroup, $socketname, $size, $status) = split(" ", $line);
                    if ($bank =~ m/\d/) {
                        $hwRamStatus = 0 if ($status !~ /^OK$/);
                    }
                }
            }

            $obj->{-value} = $hwRamMax    if ($obj->{-name} =~ m/\.hwRamMax$/);
            $obj->{-value} = $hwRamStatus if ($obj->{-name} =~ m/\.hwRamStatus$/);
        }
    }
    return;
}

1;

