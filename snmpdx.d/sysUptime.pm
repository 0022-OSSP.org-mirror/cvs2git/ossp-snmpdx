##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  sysUptime.pm: probe for System Uptime
##

package My::Probe::sysUptime;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.system.sysUptime");
}

sub probe ($$) {
    my ($self, $obj) = @_;
    
    #   query system via uptime(1)
    my $uptime = "uptime";
    if ($self->{-ctx}->{-platform}->id() =~ m/(FreeBSD|Linux|SunOS)/i) {
        $uptime = "/usr/bin/uptime"
    }
    my $out = $self->{-ctx}->{-sys}->run($uptime, "1m");

    #   parse out details (example outputs follow)
    #   FreeBSD: "5:36PM  up 6 days, 54 mins, 9 users, load averages: 0.00, 0.02, 0.00"
    #   Linux:   "17:36:50 up 49 days,  8:16,  2 users,  load average: 0.00, 0.00, 0.00"
    #   SunOS:   "5:36pm  up 71 day(s),  3:29,  2 users,  load average: 0.16, 0.11, 0.13"
    my ($days, $hours, $mins) = (0, 0, 0);
    my $spec = $out->{-stdout};
    $spec =~ s|^.*\s+up\s+||s;
    $spec =~ s|,\s+\d+\s+users.*$||s;
    $spec =~ s/(\d+)\s+day(s|\(s\))?/$days = $1, ''/se;
    $spec =~ s/(\d+):(\d{2})/$hours = $1, $mins = $2, ''/se;
    $spec =~ s/(\d+)\s+min(s|\(s\))?/$mins = $1, ''/se;

    #   provide results
    my $t = 0;
    $t += ($days  * 24 * 60 * 60);
    $t += ($hours *      60 * 60);
    $t += ($mins  *           60);
    $t *= 100;
    $obj->{-value} = $t;
    return;
}

1;

