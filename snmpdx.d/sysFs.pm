##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  sysFs.pm: probe for System, Filesystem
##

package My::Probe::sysFs;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.system.sysFs.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;

    if ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i) {
    	my @sysFsENTRY;
        my $n = 0;
        
        #   local workspace
		my $out; my $raw; my $n2i; my @dat; my $new;

        #  	process df
        $out = $self->{-ctx}->{-sys}->run("/bin/df -m", "1m");
        $raw = $out->{-stdout};
		$n2i = {};
		if ($raw =~ m/^([^\n]*\bFilesystem\b[^\n]*)\n(.*)$/s) {
			$n2i = &mkn2i(split / +/, $1);
			foreach $line (split "\n", $2) {
				@dat = split(/ +/, $line);
				if (@dat[$n2i->{Filesystem}] =~ m|^/dev/|) {
					$new = {};
                    $new->{sysFsIndex} = $n;
					if (defined $n2i->{Filesystem}) {
						$new->{sysFsDevice} = @dat[$n2i->{Filesystem}];
					}
					if (defined $n2i->{Mounted}) {
						$new->{sysFsMountpoint} = @dat[$n2i->{Mounted}];
					}
					if (defined $n2i->{_1M_blocks}) {
						$new->{sysFsMax} = @dat[$n2i->{_1M_blocks}];
					}
					if (defined $n2i->{Avail}) {
						$new->{sysFsFree} = @dat[$n2i->{Avail}];
					}
					if (defined $new->{sysFsMax} and defined $new->{sysFsFree}) {
						$new->{sysFsUsed} = $new->{sysFsMax} - $new->{sysFsFree};
					}
					$sysFsENTRY[$n++] = $new;
				}
			}
        }

        if ($obj->{-name} =~ m/\.sysFsENTRY\.([^.]+)\.(\d+)$/) {
            my $col = $1;
            my $row = ($2 > 0 ? $2 - 1 : 0);
            $self->{-ctx}->{-log}->printf(4, "col=%s row=%s", $col, $row);
            $obj->{-value} = $sysFsENTRY[$row]->{$col} if (defined $sysFsENTRY[$row] and defined $sysFsENTRY[$row]->{$col});
        }
    }
    elsif ($self->{-ctx}->{-platform}->id() =~ m/Linux/i) {
    	my @sysFsENTRY;
        my $n = 0;
        
        #   local workspace
		my $out; my $raw; my $n2i; my @dat; my $new;

        #  	process df
        $out = $self->{-ctx}->{-sys}->run("/bin/df -m", "1m");
        $raw = $out->{-stdout};
		$n2i = {};
		if ($raw =~ m/^([^\n]*\bFilesystem\b[^\n]*)\n(.*)$/s) {
			$n2i = &mkn2i(split / +/, $1);
			foreach $line (split "\n", $2) {
				@dat = split(/ +/, $line);
				if (@dat[$n2i->{Filesystem}] =~ m|^/dev/|) {
					$new = {};
                    $new->{sysFsIndex} = $n;
					if (defined $n2i->{Filesystem}) {
						$new->{sysFsDevice} = @dat[$n2i->{Filesystem}];
					}
					if (defined $n2i->{Mounted}) {
						$new->{sysFsMountpoint} = @dat[$n2i->{Mounted}];
					}
					if (defined $n2i->{_1M_blocks}) {
						$new->{sysFsMax} = @dat[$n2i->{_1M_blocks}];
					}
					if (defined $n2i->{Available}) {
						$new->{sysFsFree} = @dat[$n2i->{Available}];
					}
					if (defined $new->{sysFsMax} and defined $new->{sysFsFree}) {
						$new->{sysFsUsed} = $new->{sysFsMax} - $new->{sysFsFree};
					}
					$sysFsENTRY[$n++] = $new;
				}
			}
        }

        if ($obj->{-name} =~ m/\.sysFsENTRY\.([^.]+)\.(\d+)$/) {
            my $col = $1;
            my $row = ($2 > 0 ? $2 - 1 : 0);
            $self->{-ctx}->{-log}->printf(4, "col=%s row=%s", $col, $row);
            $obj->{-value} = $sysFsENTRY[$row]->{$col} if (defined $sysFsENTRY[$row] and defined $sysFsENTRY[$row]->{$col});
        }
    }
    elsif($self->{-ctx}->{-platform}->id() =~ m/SunOS/i) {
    	my @sysFsENTRY;
        my $n = 0;
        
        #   local workspace
		my $out; my $raw; my $n2i; my @dat; my $new;

        #  	process df
        $out = $self->{-ctx}->{-sys}->run("/bin/df -k", "1m");
        $raw = $out->{-stdout};
		$n2i = {};
		if ($raw =~ m/^([^\n]*\bFilesystem\b[^\n]*)\n(.*)$/s) {
			$n2i = &mkn2i(split / +/, $1);
			foreach $line (split "\n", $2) {
				@dat = split(/ +/, $line);
				if (@dat[$n2i->{Filesystem}] =~ m|^/dev/|) {
					$new = {};
                    $new->{sysFsIndex} = $n;
					if (defined $n2i->{Filesystem}) {
						$new->{sysFsDevice} = @dat[$n2i->{Filesystem}];
					}
					if (defined $n2i->{Mounted}) {
						$new->{sysFsMountpoint} = @dat[$n2i->{Mounted}];
					}
					if (defined $n2i->{kbytes}) {
						$new->{sysFsMax} = int(@dat[$n2i->{kbytes}] / 1024);
					}
					if (defined $n2i->{avail}) {
						$new->{sysFsFree} = int(@dat[$n2i->{avail}] / 1024);
					}
					if (defined $new->{sysFsMax} and defined $new->{sysFsFree}) {
						$new->{sysFsUsed} = $new->{sysFsMax} - $new->{sysFsFree};
					}
					$sysFsENTRY[$n++] = $new;
				}
			}
        }

        if ($obj->{-name} =~ m/\.sysFsENTRY\.([^.]+)\.(\d+)$/) {
            my $col = $1;
            my $row = ($2 > 0 ? $2 - 1 : 0);
            $self->{-ctx}->{-log}->printf(4, "col=%s row=%s", $col, $row);
            $obj->{-value} = $sysFsENTRY[$row]->{$col} if (defined $sysFsENTRY[$row] and defined $sysFsENTRY[$row]->{$col});
        }
    }
    return;
}

# name to index
sub mkn2i ($) {
    my (@name) = @_;
    my $rc = {};
    my $i = 0;
    foreach my $f (@name) {
        $f =~ s/[^\w]/_/;
        $f =~ s/^([^a-zA-Z_])/_$1/;
        $rc->{$f} = $i++;
    };
    return $rc;
}

1;

