##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  swOs.pm: probe for Software, Operating System
##

package My::Probe::swOs;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.software.swOs.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;

    #   query system via uname(1)
    my $uname = "uname";
    if    ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i)       { $uname = "/usr/bin/uname"; }
    elsif ($self->{-ctx}->{-platform}->id() =~ m/(Linux|SunOS)/i) { $uname = "/bin/uname";     }
    my $out1 = $self->{-ctx}->{-sys}->run("$uname -s 2>/dev/null || echo 'Unix'", "1d");
    my $out2 = $self->{-ctx}->{-sys}->run("$uname -r 2>/dev/null || $uname -v 2>/dev/null", "1d");
    my $name = $out1->{-stdout}; $name =~ s|^\s*(.+?)\s*$|$1|s;
    my $vers = $out2->{-stdout}; $vers =~ s|^\s*(.+?)\s*$|$1|s;

    #   provide results
    if    ($obj->{-name} =~ m|\.swOsName$|)    { $obj->{-value} = $name; }
    elsif ($obj->{-name} =~ m|\.swOsVersion$|) { $obj->{-value} = $vers; }
    return;
}

1;

