##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  hwFan.pm: probe for Hardware, Fan
##

package My::Probe::hwFan;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.hardware.hwFan.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;

    if ($self->{-ctx}->{-platform}->id() =~ m/FreeBSD/i) {
        my @hwFanENTRY; #N/A
    }
    elsif ($self->{-ctx}->{-platform}->id() =~ m/Linux/i) {
        my @hwFanENTRY; #N/A
    }
    elsif($self->{-ctx}->{-platform}->id() =~ m/SunOS/i) {
        my @hwFanENTRY;
        my $n = 0;

        #   local workspace
        my $out; my $raw; my $arch; my $new;

        #   initialize arch
        $arch = $self->{-ctx}->{-platform}->arch();

        #   hwFanENTRY
        $out = $self->{-ctx}->{-sys}->run("/usr/platform/$arch/sbin/prtdiag -v", "1m");
        $raw = $out->{-stdout};
        if ($raw =~ m/\n=+ Environmental Status =+\n.*\nFan Bank.:[^\n]*\n(.*?)\n=+/s) {
            $raw = $1;
            foreach $line (split "\n", $raw) {
                if ($line =~ m/([A-Z]+)\s+(\d+)\s+(OK|ERROR)/) {
                    my $new = {};
                    $new->{hwFanIndex} = $n;
                    $new->{hwFanId}    = "Fan_$1";
                    $new->{hwFanSpeed} = "$2";
                    if ($3 =~ m/^OK$/) {
                        $new->{hwFanStatus} = 1;
                    }
                    else {
                        $new->{hwFanStatus} = 0;
                    }
                    $hwFanENTRY[$n++] = $new;
                }
            }
        }

        if ($obj->{-name} =~ m/\.hwFanENTRY\.([^.]+)\.(\d+)$/) {
            my $col = $1;
            my $row = ($2 > 0 ? $2 - 1 : 0);
            $self->{-ctx}->{-log}->printf(4, "col=%s row=%s", $col, $row);
            $obj->{-value} = $hwFanENTRY[$row]->{$col} if (defined $hwFanENTRY[$row] and defined $hwFanENTRY[$row]->{$col});
        }
    }
    return;
}

1;

