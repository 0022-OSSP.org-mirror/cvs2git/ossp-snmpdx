##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpdx, a SNMP daemon extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  swOp.pm: probe for Software, OpenPKG
##

package My::Probe::swOp;
our @ISA = qw(My::Probe);

sub oids ($) {
    my ($self) = @_;
    return $self->{-ctx}->{-mib}->oids("*.snmpdx.host.software.swOp.*");
}

sub probe ($$) {
    my ($self, $obj) = @_;
    my $openpkg = $self->{-ctx}->{-opt}->{openpkg};

    my $rpm;
    if    ( -x "$openpkg/bin/openpkg" and -x "$openpkg/libexec/openpkg/rpm" ) { #OpenPKG 2.x
        $rpm="$openpkg/bin/openpkg rpm"
    }
    elsif ( -x "$openpkg/bin/rpm" and not -x "$openpkg/libexec/openpkg/rpm" ) { #OpenPKG 1.x
        $rpm="$openpkg/bin/rpm"
    }
    else { return; } #no (known) OpenPKG

    #   query details via rpm(1)
    my $out = $self->{-ctx}->{-sys}->run(
        "$rpm -qa --qf '[%{NAME} %{VERSION} %{RELEASE} %{BUILDTIME} %{INSTALLTIME}\\n]'", "10m");
    return if ($out->{-stdout} eq '');

    #   parse details into internal structure
    my $pkg = [];
    foreach my $entry (sort(split(/\n/, $out->{-stdout}))) {
        my ($p_n, $p_v, $p_r, $p_bt, $p_it) = split(/\s+/, $entry);
        next if ($p_n eq 'gpg-key');
        my $p = { -name => $p_n, -version => $p_v, -release => $p_r, -built => $p_bt, -installed => $p_it };
        push(@{$pkg}, $p);
    }

    #   provide result
    $self->{-ctx}->{-log}->printf(4, "name=%s", $obj->{-name});
    if ($obj->{-name} =~ m|\.swOpVersion$|) {
        #   OpenPKG instance *main* version
        my %release = ();
        foreach my $p (@{$pkg}) {
            my $r = $p->{-release};
            $r =~ s|^(\d+\.d+)\.\d+$|$1|s;
            $r =~ s|^\d{8}$|CURRENT|s;
            $release{$r}++;
        }
        my $release = (reverse sort { $release{$a} <=> $release{$b} } keys(%release))[0];
        $obj->{-value} = "OpenPKG-$release";
    }
    elsif ($obj->{-name} =~ m|\.swOpPkgENTRY\.([^.]+)\.(\d+)$|) {
        #   OpenPKG package list
        my $col = $1;
        my $row = ($2 > 0 ? $2 - 1 : 0);
        $self->{-ctx}->{-log}->printf(4, "col=%s row=%s", $col, $row);
        if (defined($pkg->[$row])) {
            if ($col eq 'swOpPkgIndex') {
                $obj->{-value} = $row;
            }
            elsif ($col eq 'swOpPkgName') {
                $obj->{-value} = $pkg->[$row]->{-name};
            }
            elsif ($col eq 'swOpPkgVersion') {
                $obj->{-value} = $pkg->[$row]->{-version};
            }
            elsif ($col eq 'swOpPkgRelease') {
                $obj->{-value} = $pkg->[$row]->{-release};
            }
            elsif ($col eq 'swOpPkgBuilt') {
                local ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) =
                    localtime($pkg->[$row]->{-built});
                $obj->{-value} = $self->{-ctx}->{-enc}->dat_encode(
                    sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $year+1900, $mon+1, $mday, $hour, $min, $sec));
            }
            elsif ($col eq 'swOpPkgInstalled') {
                local ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) =
                    localtime($pkg->[$row]->{-installed});
                $obj->{-value} = $self->{-ctx}->{-enc}->dat_encode(
                    sprintf("%04d-%02d-%02d %02d:%02d:%02d",
                        $year+1900, $mon+1, $mday, $hour, $min, $sec));
            }
        }
    }
    return;
}

1;

