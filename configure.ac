##
##  OSSP snmpdx - SNMP Daemon Extension
##  Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>
##  Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>
##
##  This file is part of OSSP snmpd, a SNMP Daemon Extension which
##  can be found at http://www.ossp.org/pkg/tool/snmpdx/.
##
##  This program is free software; you can redistribute it and/or
##  modify it under the terms of the GNU General Public  License
##  as published by the Free Software Foundation; either version
##  2.0 of the License, or (at your option) any later version.
##
##  This program is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
##  General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with this file; if not, write to the Free Software
##  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
##  USA, or contact Ralf S. Engelschall <rse@engelschall.com>.
##
##  configure.ac: auto-configuration source (syntax: Autoconf)
##

dnl # standard Autoconf prolog
AC_PREREQ(2.61)
AC_INIT

dnl # standard OSSP style header
V=`./shtool version -ltxt -dlong VERSION`
./shtool echo -e "Configuring %BOSSP snmpdx%b, Version %B${V}%b"
echo "Copyright (c) 2003-2007 Ralf S. Engelschall <rse@engelschall.com>"
echo "Copyright (c) 2003-2007 The OSSP Project <http://www.ossp.org/>"
echo "Copyright (c) 2003-2005 Cable & Wireless <http://www.cw.com/>"

dnl # check for OpenPKG
AC_MSG_CHECKING(for openpkg instance)
AC_ARG_WITH(openpkg,dnl
[  --with-openpkg=PATH     force the use of a particular OpenPKG instance],
with_openpkg="$withval",
dnl # find any OpenPKG 2.x
if test ".$with_openpkg" = .; then
    for i in `cat </etc/openpkg` /openpkg; do
        if test -x $i/bin/openpkg -a -x $i/libexec/openpkg/rpm; then
            with_openpkg="$i"
            break;
        fi
    done
fi
dnl # find any OpenPKG 1.x
if test ".$with_openpkg" = .; then
    for i in `cat </etc/openpkg` /cw; do
        if test -x $i/bin/rpm -a ! -x $i/libexec/openpkg/rpm; then
            with_openpkg="$i"
            break;
        fi
    done
fi
dnl # take first configured
if test ".$with_openpkg" = .; then
    with_openpkg="`sed </etc/openpkg -n -e 1p`"
fi
)dnl
openpkg="$with_openpkg"
AC_SUBST(openpkg)
AC_MSG_RESULT($openpkg)

dnl # check for Perl
AC_MSG_CHECKING(for perl program)
AC_ARG_WITH(perl,dnl
[  --with-perl=PATH        force the use of a particular Perl program],
with_perl="$withval",
if test ".$with_perl" = .; then
    with_perl=`./shtool path -m perl5 perl`
fi
)dnl
PATH_PERL="$with_perl"
AC_SUBST(PATH_PERL)
AC_MSG_RESULT($PATH_PERL)
PATH_POD2MAN=`echo "$PATH_PERL" | sed -e 's;/[[^/]]*$;/pod2man;'`
AC_SUBST(PATH_POD2MAN)

dnl # check for net-snmp
AC_MSG_CHECKING(for net-snmp prefix)
AC_ARG_WITH(snmp,dnl
[  --with-snmp=PREFIX      force the use of a particular net-snmp installation],
snmp_prefix="$withval",
if test ".$snmp_prefix" = .; then
    snmp_prefix=`(net-snmp-config --prefix) 2>/dev/null`
fi
)dnl
if test ".$snmp_prefix" = .; then
    AC_MSG_ERROR([path to net-snmp package not found (specify with --with-snmp=PREFIX])
fi
snmp_mibdir=`find $snmp_prefix/share/ -type d -name "mibs" -print 2>/dev/null | head -1 | sed -e 's;//*;/;g'`
if test ".$snmp_mibdir" = .; then
    snmp_mibdir=`find $snmp_prefix/ -type d -name "mibs" -print 2>/dev/null | head -1 | sed -e 's;//*;/;g'`
fi
if test ".$snmp_mibdir" = .; then
    AC_MSG_ERROR([net-snmp MIB directory not found under $snmp_prefix])
fi
snmpmibdir="$snmp_mibdir"
AC_SUBST(snmpmibdir)
AC_MSG_RESULT($snmpmibdir)

dnl # check for make(1)
AC_SET_MAKE

dnl # standard Autoconf epilog
AC_CONFIG_FILES([Makefile])
AC_OUTPUT

